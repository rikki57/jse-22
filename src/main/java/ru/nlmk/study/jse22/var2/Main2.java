package ru.nlmk.study.jse22.var2;

import ru.nlmk.study.jse22.var2.repository.IUserRepository;
import ru.nlmk.study.jse22.var2.repository.UserRepositoryJsonImpl;
import ru.nlmk.study.jse22.var2.repository.UserRepositorySimpleSerializationImpl;

public class Main2 {
    public static void main(String[] args) {
        IUserRepository userRepository = new UserRepositoryJsonImpl("jsonUsers.save");
        System.out.println(userRepository.getAll());
    }
}
