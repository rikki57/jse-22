package ru.nlmk.study.jse22.var2;

import ru.nlmk.study.jse22.var2.model.Home;
import ru.nlmk.study.jse22.var2.model.Person;
import ru.nlmk.study.jse22.var2.repository.IUserRepository;
import ru.nlmk.study.jse22.var2.repository.UserRepositoryJsonImpl;
import ru.nlmk.study.jse22.var2.repository.UserRepositorySimpleSerializationImpl;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Person igor = new Person(1L, "Igor", "Petrov", 20, new Home("Palenaya, 54-12"),
                Arrays.asList(2,4,5));
        Person alex = new Person(2L, "Alex", "Petrov", 20, new Home("Palenaya, 54-12"),
                Arrays.asList(2,3,5));
        Person ivan = new Person(3L, "Ivan", "Petrov", 20, new Home("Palenaya, 54-14"),
                Arrays.asList(2,5,5));
        Person petr = new Person(4L, "Petr", "Petrov", 20, new Home("Palenaya, 54-15"),
                Arrays.asList(2,6,5));

        IUserRepository userRepository = new UserRepositoryJsonImpl("jsonUsers.save");
        userRepository.addPerson(alex);
        userRepository.addPerson(petr);
        System.out.println(userRepository.getAll());
    }
}
