package ru.nlmk.study.jse22.var2.repository;

import ru.nlmk.study.jse22.var2.model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractUserRepository implements IUserRepository{
    protected Map<Long, Person> storage;
    protected final String fileName;
    protected static final String DEFAULT_FILE_NAME = "persons.save";

    public AbstractUserRepository() {
        this(DEFAULT_FILE_NAME);
    }

    public AbstractUserRepository(String fileName) {
        this.fileName = fileName;
        storage = new HashMap<>();
    }

    @Override
    public boolean addPerson(Person person) {
        boolean result = storage.putIfAbsent(person.getId(), person) != null;
        saveStorage();
        return result;
    }

    @Override
    public boolean removeById(Long id) {
        boolean result = storage.remove(id) != null;
        saveStorage();
        return result;
    }

    @Override
    public void clear() {
        storage.clear();
        saveStorage();
    }

    @Override
    public Person getById(Long id) {
        return storage.get(id);
    }

    @Override
    public boolean updatePerson(Person person) {
        if (storage.get(person.getId()) != null){
            storage.put(person.getId(), person);
            saveStorage();
            return true;
        }
        return false;
    }

    @Override
    public List<Person> getAll() {
        return new ArrayList<>(storage.values());
    }

    protected abstract void saveStorage();

    protected abstract void updateStorage();
}
