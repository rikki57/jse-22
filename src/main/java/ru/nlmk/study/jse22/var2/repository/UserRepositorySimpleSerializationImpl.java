package ru.nlmk.study.jse22.var2.repository;

import ru.nlmk.study.jse22.var2.model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepositorySimpleSerializationImpl extends AbstractUserRepository {
    public UserRepositorySimpleSerializationImpl(String fileName) {
        super(fileName);
        updateStorage();
    }

    protected void saveStorage(){
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))){
            objectOutputStream.writeObject(storage);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void updateStorage(){
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))){
            storage = (Map) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
