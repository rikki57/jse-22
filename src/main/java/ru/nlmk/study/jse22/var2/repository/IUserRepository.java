package ru.nlmk.study.jse22.var2.repository;

import ru.nlmk.study.jse22.var2.model.Person;

import java.util.List;

public interface IUserRepository {
    boolean addPerson(Person person);
    boolean removeById(Long id);
    void clear();
    Person getById(Long id);
    boolean updatePerson(Person person);
    List<Person> getAll();
}
