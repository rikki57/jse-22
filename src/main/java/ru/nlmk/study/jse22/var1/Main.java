package ru.nlmk.study.jse22.var1;

import java.io.*;

public class Main {
    private static final String FILE_NAME = "fileToRead.txt";

    public static void main(String[] args) {
        BufferedReader bufferedReader  = null;
        try{
            bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            String line;
            while ((line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
