package ru.nlmk.study.jse22.var1;

import java.io.*;

public class Main2 {
    private static final String FILE_NAME = "fileToRead.txt";

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(FILE_NAME)));) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
