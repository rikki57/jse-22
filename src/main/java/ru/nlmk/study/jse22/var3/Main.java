package ru.nlmk.study.jse22.var3;

import java.io.File;
import java.io.FilenameFilter;

public class Main {
    public static void main(String[] args) {
        File file = new File("D:/Test");
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".save");
            }
        };
        File[] files = file.listFiles(filter);
        for (int i = 0; i < files.length; i++){
            System.out.println(files[i].getName());
        }
    }
}
